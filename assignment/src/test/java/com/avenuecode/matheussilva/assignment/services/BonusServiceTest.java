package com.avenuecode.matheussilva.assignment.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import com.avenuecode.matheussilva.assignment.models.Funcionario;

class BonusServiceTest {

	private BonusService service;

	@Test
	void bonusDeveriaSerZeroParaFuncionarioComSalarioMuitoAlto() {
		try {
			this.service = new BonusService();
			Funcionario funcionario = criarFuncionario(new BigDecimal("25000"));
			
			service.calcularBonus(funcionario);
			
			fail("nao deu exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Funcionario com salario maior do que R$10000 nao pode receber bonus!", e.getMessage());
		}

	}

	@Test
	void bonusDeveriaSer10PorCentoDoSalario() {
		this.service = new BonusService();
		Funcionario funcionario = criarFuncionario(new BigDecimal("2500"));
		
		BigDecimal bonus = service.calcularBonus(funcionario);
		
		assertEquals(new BigDecimal("250.00"), bonus);
	}

	@Test
	void bonusDeveriaSerDezPorCentoParaSalarioDeExatamente10000() {
		this.service = new BonusService();
		Funcionario funcionario = criarFuncionario(new BigDecimal("10000"));
		
		BigDecimal bonus = service.calcularBonus(funcionario);

		assertEquals(new BigDecimal("1000.00"), bonus);
	}

	private Funcionario criarFuncionario(BigDecimal salario) {
		return new Funcionario("Matheus", salario);
	}

}
