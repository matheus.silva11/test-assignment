package com.avenuecode.matheussilva.assignment.acceptance.steps;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import com.avenuecode.matheussilva.assignment.models.Desempenho;
import com.avenuecode.matheussilva.assignment.models.Funcionario;
import com.avenuecode.matheussilva.assignment.services.ReajusteService;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AtualizandoFuncionarioSalarioComDesempenho {
	
	Funcionario funcionario;
	
	@Given("Um funcionario valido com salario valido")
	public void um_funcionario_valido() {
		this.funcionario = new Funcionario("Matheus", new BigDecimal("2000.00"));		
	}


	@When("Calcula o bonus com base no desempenho A_DESEJAR")
	public void o_nome_mudar() {
		ReajusteService service = new ReajusteService();
		service.concederReajuste(funcionario, Desempenho.A_DESEJAR);
	}
	
	@Then("Deve retornar o valor correto")
	public void salva_o_novo_nome_do_funcionario() {
		assertEquals(new BigDecimal("2060.00"), funcionario.getSalario());
	}


}
