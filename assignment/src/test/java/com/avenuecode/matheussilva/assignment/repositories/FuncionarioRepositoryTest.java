package com.avenuecode.matheussilva.assignment.repositories;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.avenuecode.matheussilva.assignment.models.Funcionario;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class FuncionarioRepositoryTest {
	
	@Autowired
	private FuncionarioRepository funcionarioRepository;
	
	@Test
	public void insertFuncionario() {
		Funcionario funcionario = new Funcionario("Matheus", new BigDecimal("10000"));
		funcionarioRepository.save(funcionario);
		
		Integer countUser = funcionarioRepository.findAll().size();
		
		assertEquals(1, countUser);
	}
	
	@Test
	public void checkSavedFuncionarioPassingOtherName() {
		Funcionario funcionario = new Funcionario("Matheus", new BigDecimal("10000"));
		funcionarioRepository.save(funcionario);
		
		Integer countUser = funcionarioRepository.findAll().size();
		
		assertEquals(1, countUser);
		
		Funcionario funcInvalido = funcionarioRepository.findFuncionarioByNome("Joao");
		
		assertNull(funcInvalido);
	}
	
	@Test
	public void updateSalarioFuncionario() {
		Funcionario funcionario = new Funcionario("Matheus", new BigDecimal("10000"));
		funcionarioRepository.save(funcionario);
		funcionario = funcionarioRepository.findFuncionarioByNome("Matheus");
		funcionario.setSalario(new BigDecimal("20000"));
		
		funcionarioRepository.save(funcionario);
		
		Integer countUser = funcionarioRepository.findAll().size();
		assertEquals(1, countUser);
	}

}
