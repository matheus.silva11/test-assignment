package com.avenuecode.matheussilva.assignment.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import com.avenuecode.matheussilva.assignment.models.Desempenho;
import com.avenuecode.matheussilva.assignment.models.Funcionario;

public class ReajusteServiceTest {

	private ReajusteService service;
	private Funcionario funcionario;

	@Test
	void reajusteDeveriaSerDeTresPorcentoQuandoDesempenhoForADesejar() {
		this.service = new ReajusteService();
		this.funcionario = new Funcionario("Matheus", new BigDecimal("2000.00"));
		
		service.concederReajuste(funcionario, Desempenho.A_DESEJAR);

		assertEquals(new BigDecimal("2060.00"), funcionario.getSalario());
	}

	@Test
	void reajusteDeveriaSerDeQuinzePorcentoQuandoDesempenhoForBom() {
		this.service = new ReajusteService();
		this.funcionario = new Funcionario("Matheus", new BigDecimal("2000.00"));
		
		service.concederReajuste(funcionario, Desempenho.BOM);
		
		assertEquals(new BigDecimal("2300.00"), funcionario.getSalario());
	}

	@Test
	void reajusteDeveriaSerDeVintePorcentoQuandoDesempenhoForOtimo() {
		this.service = new ReajusteService();
		this.funcionario = new Funcionario("Matheus", new BigDecimal("2000.00"));
		
		service.concederReajuste(funcionario, Desempenho.OTIMO);

		assertEquals(new BigDecimal("2400.00"), funcionario.getSalario());
	}

}
