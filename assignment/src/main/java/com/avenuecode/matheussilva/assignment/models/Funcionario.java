package com.avenuecode.matheussilva.assignment.models;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "funcionario")
public class Funcionario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "funcionario_id")
	private Long id;

	@NotNull
	@Column(name = "username")
	private String nome;

	@NotNull
	@Column(name = "salary")
	private BigDecimal salario;

	public Funcionario() {
		super();
	}

	public Funcionario(String nome, BigDecimal salario) {
		this.nome = nome;
		this.salario = salario;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getSalario() {
		return salario;
	}

	public void setSalario(BigDecimal salario) {
		this.salario = salario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	public void reajustarSalario(BigDecimal percentual) {
		BigDecimal aumento = calcularValorDoAumento(percentual);
		this.salario = this.salario.add(aumento).setScale(2, RoundingMode.HALF_UP);
	}

	private BigDecimal calcularValorDoAumento(BigDecimal percentual) {
		return salario.multiply(percentual);
	}

}
