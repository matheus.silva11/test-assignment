package com.avenuecode.matheussilva.assignment.services;

import java.math.BigDecimal;

import com.avenuecode.matheussilva.assignment.models.Desempenho;
import com.avenuecode.matheussilva.assignment.models.Funcionario;

public class ReajusteService {

	public void concederReajuste(Funcionario funcionario, Desempenho desempenho) {
		BigDecimal reajuste = desempenho.percentualReajuste();
		funcionario.reajustarSalario(reajuste);
	}

}
