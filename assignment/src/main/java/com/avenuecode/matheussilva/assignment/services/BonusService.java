package com.avenuecode.matheussilva.assignment.services;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Service;

import com.avenuecode.matheussilva.assignment.models.Funcionario;

@Service
public class BonusService {

	public BigDecimal calcularBonus(Funcionario funcionario) {
		BigDecimal valor = funcionario.getSalario().multiply(new BigDecimal("0.1"));

		if (valor.compareTo(new BigDecimal("1000")) > 0) {
			throw new IllegalArgumentException("Funcionario com salario maior do que R$10000 nao pode receber bonus!");
		}

		return valor.setScale(2, RoundingMode.HALF_UP);
	}

}
