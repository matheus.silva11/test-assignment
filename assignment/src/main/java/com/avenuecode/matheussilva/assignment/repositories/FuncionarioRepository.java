package com.avenuecode.matheussilva.assignment.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.avenuecode.matheussilva.assignment.models.Funcionario;

@Repository
public interface FuncionarioRepository extends JpaRepository<Funcionario, Long>{
	
	public List<Funcionario> findAll();
	
	public Funcionario findFuncionarioByNome(String nome);

}
